# Manual security testing of [GitLab](https://gitlab.com/)

## Test #1: Forget password
OWASP Cheat Sheet: [Forgot Password](https://cheatsheetseries.owasp.org/cheatsheets/Forgot_Password_Cheat_Sheet.html)

| Test step                                         | Expected result     | Actual result                       |
| ------------------------------------------------- | ------------------- | ------------------------------------|
| Open [gitlab.com/](https://gitlab.com/) | Welcome page opened | Ok, Welcome page opened |
| Click the Sign In button in the header  | Signing page opened | OK, Signing page opened |
| Click on "Forgot your password?" link under password field  | Page asks recovery email | Ok, new page with email input field and captcha opened |
| Enter an existing email | Consistent message for both existent and non-existent accounts, message come to email address | Ok, message window "If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes" appeared, letter with recovery instructions came |
| Enter a random non-existing email | Consistent message for both existent and non-existent accounts | Ok, message window "If your email address exists in our database, you will receive a password recovery link at your email address in a few minutes" appeared |
| Enter `'; DROP TABLE users; --` | Error message | Ok, error "Please provide a valid email address.Please provide a valid email address" appeared |
| On Signing page enter existing email and its valid password, press "Sign in" | Website do not change account info until a valid token is presented | Ok, old valid account credentials allow to sing in to the account |

All tests passed, the following recommendations are implemented:
* Return a consistent message for both existent and non-existent accounts.
* Ensure that the time taken for the user response message is uniform.
* Use a side-channel to communicate the method to reset their password.
* Do not make a change to the account until a valid token is presented, such as locking out the accountv


## Test #2: SQL Injection
OWASP Cheat Sheet: [SQL Injection Prevention](https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html)

| Test step                                         | Expected result     | Actual result                       |
| ------------------------------------------------- | ------------------- | ------------------------------------|
| Open [gitlab.com/](https://gitlab.com/) | Welcome page opened | Ok, Welcome page opened |
| Click the Sign In button in the header  | Signing page opened | OK, Signing page opened |
| Sign in with existing email and its valid password, press "Sign in" | Successful sigh in and transfer to main user page | Ok, Authentication is successful, main user page opened |
| Click on "Filter by name" field and enter `'; DROP TABLE users; --` | No DB updates | Ok, service is alive and users are available |
| Click on "Search in GitLab" field and enter `'; DROP TABLE users; --` | No DB updates | Ok, service is alive and users are available. The result of query is "We couldn't find any projects matching '; DROP TABLE users; --" |
| Return to main user page and click "New project". In project's name input field write `'; DROP TABLE users; --` | No DB updates | Ok, service is alive and users are available. The error is "Name can contain only lowercase or uppercase letters, digits, emojis, spaces, dots, underscores, dashes, or pluses." |


All tests passed, the service is free from SQL Injection (at least in tested scope)


## Test #3: XSS
OWASP Cheat Sheet: [Cross Site Scripting Prevention](https://cheatsheetseries.owasp.org/cheatsheets/Cross_Site_Scripting_Prevention_Cheat_Sheet.html)

| Test step                                         | Expected result     | Actual result                       |
| ------------------------------------------------- | ------------------- | ------------------------------------|
| Open [gitlab.com/](https://gitlab.com/) | Welcome page opened | Ok, Welcome page opened |
| Click the Sign In button in the header  | Signing page opened | OK, Signing page opened |
| Sign in with existing email and its valid password, press "Sign in" | Successful sigh in and transfer to main user page | Ok, Authentication is successful, main user page opened |
| Click on "Filter by name" field and enter `<script>alert("Vulnerability")</script>` | No XSS occured | Ok, no "Vulnerability" message occured |
| Click on "Search in GitLab" field and enter `<script>alert("XSS Attack!")</script>` | No XSS occured | Ok, no "Vulnerability" message occured. The result of query is "We couldn't find any projects matching" |
| Return to main user page and click "New project". In project's name input field write `<script>alert("Vulnerability")</script>` | No XSS occured | Ok, no "Vulnerability" message occured. The error is "Name can contain only lowercase or uppercase letters, digits, emojis, spaces, dots, underscores, dashes, or pluses." |


All tests passed, the service is free from Cross-Site Scripting (at least in tested scope)
